#!/usr/bin/env node

const CodebasdCLI = require('./lib/executor')
CodebasdCLI.execute()



// Example of using the lib in a server controller or a CLI command
const GroveLib = require('path/to/lib');

// Example usage
GroveLib.Docker.deployService(serviceConfig);
GroveLib.Git.cloneRepository(repoDetails);
// ... and so on
