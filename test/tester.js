const { _, log } = require('basd')
const fs = require('fs')
const path = require('path')
const clc = require('cli-color')
const pkg = require('../package')
const prompts = require('prompts')
const { promisify } = require('util')
const { program } = require('commander')
const Handlebars = require('handlebars')
// const { spinner } = require('../lib/spinner')
const { spinner } = require('@basd/spinner')
const { spawn } = require('child_process')

class SevnStackCLI {
  constructor() {
    this.commander = new Commander
  }
  async test(...args) {
    return this.execute(['foo', 'sevn', ...args])
  }
  async execute(args) {
    return this.commander.program.parseAsync(args)
  }
  static execute(args) {
    const cli = new this
    return cli.execute(args)
  }
}

function commands() {
  // Define progam settings
  this.program
    .version(this.pkg.version, '-v, --version', 'output the current version')
    .description(this.pkg.description)

  // `create` - Command to create a new project
  this.program
    .command('create [app-name]')
    // .command('create [app-name]', { isDefault: true })
    .description('Create a new web application')
    .option('-n, --site-name', `Define the site name for the Nuxt app`)
    .option('-F, --frontend', `Use only a static Nuxt server without a backend`)
    .option('-m, --monolithic', `Use a monolithic architecture`)
    .option('-d, --decoupled', `Use a decoupled architecture`)
    .option('-s, --ssr', `Use Server-Side Rendering with Nuxt`)
    .option('-D, --docker', `Include Docker configuration`)
    .option('-o, --features [features...]', `Define the features to install`)
    .option('-NF, --no-features', `Don't install any features`)
    .option('-t, --tls', `Use TLS (HTTPS) for transport protocol`)
    .option('-y, --force', 'Use default configuration')
    // .option('-f, --force', `Force creation without going through the prompts`)
    .action(this.createCommand.bind(this))

  // `init` - Command to initialize a new project in an existing directory
  this.program
    .command('init [directory]')
    .description('Initialize a new project in an existing directory')
    .option('-y, --force', 'Use default configuration')
    .action(this.initCommand.bind(this))
}

const questions = [
  // appName
  {
    type: 'text',
    name: 'appName',
    message: 'Enter the name of your app:',
    validate: value => value ? true : 'appName is required',
    format: v => _.kebabCase(v),
  },
  // siteName
  {
    type: 'text',
    name: 'siteName',
    message: 'Enter the name of your site:',
    initial: (p, v) => _.startCase(v.appName),
  },
  // frontend
  {
    type: 'toggle',
    name: 'frontend',
    message: 'Is this just a SPA frontend?',
    initial: false,
    active: 'yes',
    inactive: 'no'
  },
  // architecture
  {
    type: prev => !prev ? 'select' : null, 
    name: 'architecture',
    message: 'Choose your application architecture:',
    choices: [
      { title: 'Monolithic', value: 'monolithic' },
      { title: 'Decoupled', value: 'decoupled' },
    ],
  },
  // nuxtMode
  {
    type: prev => !prev ? 'select' : null, 
    name: 'nuxtMode',
    message: 'Choose the Nuxt.js mode: (SPA = Single Page Application | SSR = Server Side Rendering)',
    choices: [
      { title: 'SPA', value: 'spa' },
      { title: 'SSR', value: 'ssr' },
    ],
  },
  // Advanced
  {
    type: 'confirm',
    name: 'advanced',
    message: `Do you want to continue with advanced configuration or just setup your stack now?`,
    initial: true
  },
  // siteSubtitle
  {
    type: (p, v) => v.advanced ? 'text' : null,
    name: 'siteSubtitle',
    message: 'Enter the subtitle for your site:',
    initial: '',
  },
  // description
  {
    type: (p, v) => v.advanced ? 'text' : null,
    name: 'description',
    message: 'Enter the description for your site:',
    initial: '',
  },
  // features
  {
    type: 'multiselect',
    name: 'features',
    message: 'Select additional features:',
    choices: [
      { title: 'Authentication', value: 'auth' },
      { title: 'WebSockets', value: 'sockets' },
      { title: 'PWA', value: 'pwa' },
      { title: 'Robots.txt', value: 'robots' },
      { title: 'Sitemap', value: 'sitemap' },
    ],
    min: 0,
    validate (value) {
      if (!value.every(val => this.choices.find(c => c.value === val)))
        return `Invalid features selected`
      return true
    },
  },
]

class Builder {
  constructor() {
  }
  async collectConfig(options) {
    if (options.monolithic && options.decoupled)
      throw new Error(`Site architecture can't be both monolithic and decoupled`)
    if (options.monolithic) options.architecture = 'monolithic'
    if (options.decoupled) {
      options.architecture = 'decoupled'
      options.frontend = false
    }
    if (options.ssr) options.nuxtMode = 'ssr'

    let responses = this.getDefaults(options, questions)
    if (!options.force) {
      const answers = await this.prompt(options)
      responses = _.merge(responses, answers)
    }
    // log({ responses, options })
    return this.parseConfig({ ...responses, ..._.omit(options, ['features']) })
  }
  getDefaults(options, questionsArray = questions) {
    let last, prev
    return _.transform(questionsArray, (o, item) => {
      let type = item.type
      if (_.isFunction(type))
        type = type(prev, o, last)
      if (type === null && item.choices)
        type = 'select'
      
      if (!_.isNil(options[item.name]))
        item.initial = options[item.name]
      else if (_.isFunction(item.initial))
        item.initial = item.initial(prev, o, last)

      let value = o[item.name]
      const { initial = null } = item
      if (type === 'select')
        value = initial || _.get(item, 'choices.0.value', null)
      else if (type === 'multiselect') {
        if ((_.isBoolean(initial) && initial) || (!_.isBoolean(initial) && !initial))
          value = item.choices.map(c => c.value)
        else if (_.isArray(initial))
          value = initial
        else if (_.isBoolean(initial) && !initial)
          value = []
      } else if (type === 'list') {
        value = !initial 
          ? [] 
          : _.isString(initial) 
            ? initial.split(item.separator || ' ')
            : initial
      } else value = initial

      if (item.validate) {
        const result = item.validate(value)
        if (result !== true) {
          if (_.isString(result)) throw new Error(result)
          else throw new Error(`Invalid value ${value} for option ${item.name}`)
        }
      }

      last = item
      prev = value
      o[item.name] = value
    }, {})
  }
  async prompt(options = {}, questionsArray = questions) {
    prompts.override(options)
    return new Promise((resolve, reject) => {
      prompts(questionsArray, { onCancel: () => {
        resolve(null)
      }}).then(resolve)
    })
  }
  parseConfig(config) {
    if (config.frontend) {
      config.architecture = 'monolithic'
      config.nuxtMode = 'spa'
    }
    if (config.architecture === 'monolithic') {
      config.monolithic = true
      config.decoupled = false
    } else {
      config.monolithic = false
      config.decoupled = true
    }
    delete config.advanced
    delete config.force
    delete config.frontend

    return _.pick(config, _.keys(config).sort())
  }

  prepareConfigOld() {
    const config = this.config
    config.static = config.rendering === 'static'
    config.siteNameKebab = _.kebabCase(config.siteName)
    config.monolithic = config.architecture === 'monolithic'
    config.decoupled = config.architecture === 'decoupled'
    if (config.decoupled) {
      let port = config.backendDomain.split(':')
      if (port.length === 2)
        port = port[1]
      else
        port = 3000
      config.port = port
      config.frontendUrl = `${config.protocol}://${config.frontendDomain}`
      config.backendUrl = `${config.protocol}://${config.backendDomain}`
    } else {
      config.port = 3000
      config.devUrl = `${config.protocol}://${config.devDomain}`
    }
  }
}

class Commander {
  constructor() {
    this.pkg = pkg
    this.program = program
    this.builder = new Builder
    commands.call(this)
  }
  // Commands //
  // `create`
  async createCommand(appName, options) {
    const cwd = process.cwd()
    if (!options.targetPath) {
      if (!appName) {
        options.targetPath = cwd
        appName = path.basename(cwd)
      } else
        options.targetPath = path.resolve(cwd, appName)
    }
    log(clc.green.underline(`\nCreating a new app:`), clc.greenBright(appName), `\n`)
    // log(clc.green(`In the directory:`), clc.greenBright(options.targetPath), '\n')
    const config = await this.builder.collectConfig({ ...options, appName })
    await this.create(config)
  }
  // `init`
  async initCommand(dir, options) {
    dir = dir || process.cwd()
    options.targetPath = dir
    options.appName = path.basename(dir)
    log(clc.green.underline(`\nInitiating a new app:`), clc.greenBright(options.appName))

    const config = await this.builder.collectConfig(options)
    await this.create(config)
  }
  // Utils //
  async create(config) {
    const scaffolder = new Scaffolder(config)
    await scaffolder.scaffold()
  }
}

const delay = 150 // @tmp
class Scaffolder {
  constructor(config = {}) {
    this.config = config
    this.targetPath = this.config.targetPath
    Handlebars.registerHelper('hasFeature', feature => this.config.features.includes(feature))
    this.targetPath = path.join(process.cwd(), 'tmp', _.kebabCase(this.config.siteName)) // @tmp
  }
  async scaffold() {
    spinner.start(`Starting project scaffolding...`)
    try {
      await _.sleep(delay) // @tmp
      for (let ii = 0; ii < 7; ii++) { // @tmp
        spinner.text = `Cool beans #${ii + 1}` // @tmp
        await _.sleep(100) // @tmp
      } // @tmp
      await this.createDirectories()
      await _.sleep(delay) // @tmp
      await this.processFiles()
      await _.sleep(delay) // @tmp
      await this.installDependencies()
      await _.sleep(delay) // @tmp
      spinner.succeed(`Project scaffolding complete!`)
      log(clc.green.underline(`\nYour project is ready:`), clc.greenBright(this.targetPath), `\n`)
      log(this) // @tmp
    } catch (error) {
      spinner.succeed(`Scaffold creation error: ${error.message}`)
      console.error(error)
      await this.cleanupResources()
    }
  }
  async createDirectories() {
    spinner.text = `Creating project directories...`
    // const targetPath = path.join(process.cwd(), 'tmp', _.kebabCase(this.config.siteName)) // @tmp
    // await promisify(fs.mkdir)(targetPath, { recursive: true })
    // if (this.config.decoupled) {
    //   await promisify(fs.mkdir)(path.join(targetPath, 'client'), { recursive: true })
    //   await promisify(fs.mkdir)(path.join(targetPath, 'server'), { recursive: true })
    // } else {
    //   await promisify(fs.mkdir)(path.join(targetPath, 'src'), { recursive: true })
    //   await promisify(fs.mkdir)(path.join(targetPath, 'src/api'), { recursive: true })
    // }
    // this.targetPath = targetPath
  }
  async processFiles() {
    spinner.text = `Processing templates and files...`
    // const filesToProcess = await this.prepareFilesToProcess()

    // for (const file of filesToProcess) {
    //   const srcPath = file.src.startsWith('/') ? file.src : path.join('templates', file.src)
    //   const destPath = file.dest.startsWith('/') ? file.dest : path.join(this.targetPath, file.dest)
    //   const directory = path.dirname(destPath)
    //   await promisify(fs.mkdir)(directory, { recursive: true })
    //   if (file.isTemplate) {
    //     const templateContent = await promisify(fs.readFile)(srcPath, 'utf-8')
    //     const template = Handlebars.compile(templateContent)
    //     const result = template(this.config)
    //     await promisify(fs.writeFile)(destPath, result)
    //   } else {
    //     await promisify(fs.copyFile)(srcPath, destPath)
    //   }
    // }
  }
  async prepareFilesToProcess() {
    // const nuxtPath = this.config.decoupled 
    //   ? path.join(this.targetPath, 'client') 
    //   : path.join(this.targetPath, 'src')
    // const expressPath = this.config.decoupled 
    //   ? path.join(this.targetPath, 'server') 
    //   : path.join(this.targetPath, 'src/api')

    // const filesToProcess = [
    //   { src: 'nuxt/nuxt.config.hbs', dest: path.join(nuxtPath, 'nuxt.config.js'), isTemplate: true },
    //   { src: 'nuxt/nuxt.package.hbs', dest: path.join(this.targetPath, 'package.json'), isTemplate: true },
    //   { src: 'express/express.package.hbs', dest: path.join(expressPath, 'package.json'), isTemplate: true },
    //   ...(this.config.useDocker ? [
    //     { src: 'Dockerfile.hbs', dest: 'Dockerfile', isTemplate: true },
    //     { src: 'docker-compose.yml.hbs', dest: 'docker-compose.yml', isTemplate: true },
    //   ] : []),
    // ]

    // // Add Nuxt and Express files to the list
    // const nuxtFiles = await this.getFilesRecursively(path.join('templates', 'nuxt'))
    // this.addFilesToProcess(nuxtFiles, filesToProcess, nuxtPath, 'nuxt')
    
    // const expressFiles = await this.getFilesRecursively(path.join('templates', 'express'))
    // this.addFilesToProcess(expressFiles, filesToProcess, expressPath, 'express')

    // return filesToProcess
  }
  addFilesToProcess(filePaths, filesToProcess, basePath, prefix) {
    for (const filePath of filePaths) {
      const fullFilePath = `${prefix}/${filePath}`
      const index = filesToProcess.findIndex(f => f.src === fullFilePath)
      if (index > -1) continue
      const isTemplate = filePath.endsWith('.hbs')
      const dest = path.join(basePath, !isTemplate ? filePath : filePath.slice(0, -4))
      filesToProcess.push({ src: fullFilePath, dest, isTemplate })
    }
  }
  async installDependencies() {
    if (this.config.decoupled) {
      spinner.text = `Installing client dependencies...`
      await _.sleep(delay) // @tmp
      // await this.runNpmInstall(path.join(this.targetPath, 'client'))
      spinner.text = `Installing server dependencies...`
      await _.sleep(delay) // @tmp
      // await this.runNpmInstall(path.join(this.targetPath, 'server'))
    } else {
      spinner.text = `Installing dependencies...`
      await _.sleep(delay) // @tmp
      // await this.runNpmInstall(path.join(this.targetPath, 'src'))
    }
  }
  runNpmInstall(directory) {
    return new Promise((resolve, reject) => {
      const install = spawn('npm', ['install'], { cwd: directory, stdio: 'ignore' })
      install.on('close', code => {
        if (code === 0) {
          resolve()
        } else {
          reject(`npm install failed with code ${code}`)
        }
      })
    })
  }
  async cleanupResources() {
    try {
      // await promisify(fs.rm)(this.targetPath, { recursive: true, force: true })
      log('Cleaned up incomplete project files.')
    } catch (cleanupError) {
      console.error('Error during cleanup:', cleanupError)
    }
  }
}

async function test() {
  const cli = new SevnStackCLI()
  await cli.test('create', 'foo', '-y')
  // await cli.test('create', 'foo')
  // await cli.test('create', 'foo', '--force')
  // await cli.test('create', '-h')
  // await cli.test('create', '-y')
  // await cli.test('create', '-yds')
  // await cli.test('create', '-ydtD')
  // await cli.test('init', '-y')
  // await cli.test('create', '-ymst')
  // await cli.test('create', '-ymstDo', 'auth', 'sockets')
  // await cli.test('create', '-ymstDo')
  // await cli.test('create', '-ymstDNF')
}

async function testSpinner() {
  const { Spinner } = require('./spinner')
  const { spinner } = require('./spinner')
  // const spinner = Spinner.spin({ spinner: 'dots' })
  spinner.start('Here we go')
  await _.sleep(1000)
  for (let ii = 0; ii < 33; ii++) {
    spinner.text = `Cool beans #${ii + 1}`
    await _.sleep(100)
  }
  spinner.succeed('We did it!')
}

_.executor(test)
