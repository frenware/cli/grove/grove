![Groveops Logo](docs/grove.png "Groveops")

# Groveops

> Plant some trees

[![npm](https://img.shields.io/npm/v/groveops?style=flat&logo=npm)](https://www.npmjs.com/package/groveops)
[![pipeline](https://gitlab.com/frenware/cli/grove/grove/badges/master/pipeline.svg)](https://gitlab.com/frenware/cli/grove/grove/-/pipelines)
[![license](https://img.shields.io/npm/l/groveops)](https://gitlab.com/frenware/cli/grove/grove/-/blob/master/LICENSE)
[![downloads](https://img.shields.io/npm/dw/groveops)](https://www.npmjs.com/package/groveops) 

[![Gitlab](https://img.shields.io/badge/Gitlab%20-%20?logo=gitlab&color=%23383a40)](https://gitlab.com/frenware/cli/grove/grove)
[![Github](https://img.shields.io/badge/Github%20-%20?logo=github&color=%23383a40)](https://github.com/basedwon/grove)
[![Twitter](https://img.shields.io/badge/@basdwon%20-%20?logo=twitter&color=%23383a40)](https://twitter.com/basdwon)
[![Discord](https://img.shields.io/badge/Basedwon%20-%20?logo=discord&color=%23383a40)](https://discordapp.com/users/basedwon)

Grove is a comprehensive orchestrator for software development, designed to streamline and manage development workflows. It automates the setup of Docker Compose stacks, integrates with Git services, and provides a private Docker registry, alongside robust CI/CD pipeline management. With its user-friendly web interface and powerful CLI, Grove offers a seamless experience for setting up, managing, and deploying various software projects.




Install Grove globally using npm:

```bash
npm i -g groveops
```

## Getting Started

### Initialize Grove

Set up the Grove environment in your desired directory:

```bash
grove init
```

This command creates the necessary directory structure and initializes the Grove web application.

### CLI Commands

Grove's CLI offers a variety of commands for managing your development environment:

- `grove up`: Start the main Docker Compose process and all services.
- `grove down`: Stop all running services.
- `grove add-stack [template]`: Add a new stack from a predefined template.
- `grove remove-stack [name]`: Remove an existing stack.
- `grove list-stacks`: List all the available stack templates.
- `grove update`: Update Grove to the latest version.

### Using the Web Interface

After running `grove up`, visit the Grove web interface at the specified URL (e.g., `http://localhost:3000`). The web interface provides:

- Dashboard for monitoring running services.
- Controls for managing Docker stacks, Git repositories, and CI/CD pipelines.
- Settings for custom configurations and preferences.






## Installation

Install Groveops globally via npm:

```sh
npm install -g groveops
```

## Usage

[USAGE]

## Documentation

- [API Reference](/docs/api.md)

## Tests

In order to run the test suite, simply clone the repository and install its dependencies:

```sh
git clone https://github.com/basedwon/grove.git
cd grove
npm install
```

To run the tests:

```sh
npm test
```

## Contributing

Thank you! Please see our [contributing guidelines](/docs/contributing.md) for details.

## Donations

If you find this project useful and want to help support further development, please send us some coin. We greatly appreciate any and all contributions. Thank you!

**Bitcoin (BTC):**
```
1JUb1yNFH6wjGekRUW6Dfgyg4J4h6wKKdF
```

**Monero (XMR):**
```
46uV2fMZT3EWkBrGUgszJCcbqFqEvqrB4bZBJwsbx7yA8e2WBakXzJSUK8aqT4GoqERzbg4oKT2SiPeCgjzVH6VpSQ5y7KQ
```

## License

groveops is [MIT licensed](https://gitlab.com/frenware/cli/grove/grove/-/blob/master/LICENSE).

