db.createUser(
  {
    user: "yourUsername",
    pwd: "yourPassword",
    roles: [
      {
        role: "readWrite",
        db: "yourDatabaseName"
      }
    ]
  }
);

db.newCollection.insert(
  { initialDocumentKey: "initialDocumentValue" }
);
