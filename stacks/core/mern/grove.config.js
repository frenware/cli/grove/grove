module.exports = {
  name: "MERN Stack",
  description: "A full-stack JavaScript solution with MongoDB, Express.js, React, and Node.js.",
  version: "1.0.0",
  services: {
    mongodb: {
      image: "mongo:latest",
      ports: ["27017:27017"]
    },
    express: {
      buildPath: "./docker/express",
      ports: ["3001:3001"]
    },
    react: {
      buildPath: "./docker/react",
      ports: ["3000:3000"]
    }
  },
  dependencies: ["mongodb", "express", "react"], // Dependencies between services
  setupInstructions: "Run `docker-compose up` to start the MERN stack."
}
