module.exports = {
  name: "LAMP Stack",
  description: "Basic LAMP stack with PHP 7.4, Apache, and MySQL 5.7.",
  version: "1.0.0",
  services: {
    web: {
      description: "PHP and Apache server",
      ports: ["80:80"]
    },
    db: {
      description: "MySQL 5.7 Database",
      environment: ["MYSQL_ROOT_PASSWORD=rootpassword", "MYSQL_DATABASE=mydb"]
    }
  },
  dependencies: ["web", "db"], // Dependencies between services
  setupInstructions: "Run `docker-compose up` to start the LAMP stack."
};
