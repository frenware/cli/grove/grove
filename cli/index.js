#!/usr/bin/env node

const { program } = require('commander')
const registerProjectCommands = require('./commands/projectCmds')
const registerStackCommands = require('./commands/stackCmds')
const registerDockerCommands = require('./commands/dockerCmds')
const registerGitCommands = require('./commands/gitCmds')
const registerCICDCommands = require('./commands/cicdCmds')
const registerTemplateCommands = require('./commands/templateCmds')
const registerSettingsCommands = require('./commands/settingsCmds')

// CLI configuration
program
  .name(`grove`)
  .description(`Grove: A comprehensive devops orchestrator`)
  .version(`1.0.0`)

// Registering command modules
registerProjectCommands(program)
registerStackCommands(program)
registerDockerCommands(program)
registerGitCommands(program)
registerCICDCommands(program)
registerTemplateCommands(program)
registerSettingsCommands(program)

// Parse command line arguments
program.parse(process.argv)
