const fs = require('fs')
const path = require('path')
const os = require('os')

/**
 * Checks if a given directory exists.
 * @param {string} dirPath - Path to the directory.
 * @returns {boolean} - True if exists, false otherwise.
 */
function directoryExists(dirPath) {
  try {
    return fs.statSync(dirPath).isDirectory()
  } catch (err) {
    return false
  }
}

/**
 * Create a directory if it doesn't exist.
 * @param {string} dirPath - Path to the directory.
 */
function ensureDirectoryExists(dirPath) {
  if (!directoryExists(dirPath)) {
    fs.mkdirSync(dirPath, { recursive: true })
  }
}

/**
 * Formats and prints a message to the console.
 * @param {string} message - Message to be formatted.
 * @param {string} type - Type of the message (e.g., 'error', 'info').
 */
function printMessage(message, type = 'info') {
  const prefix = type === 'error' ? 'Error: ' : 'Info: '
  console.log(`${prefix}${message}`)
}

/**
 * Resolves the home directory of the current user.
 * @returns {string} - Path to the user's home directory.
 */
function getUserHomeDirectory() {
  return os.homedir()
}

/**
 * Joins path segments and normalizes the resulting path.
 * @param {...string} segments - Path segments to join.
 * @returns {string} - The combined path.
 */
function joinAndNormalizePath(...segments) {
  return path.normalize(path.join(...segments))
}

module.exports = {
  directoryExists,
  ensureDirectoryExists,
  printMessage,
  getUserHomeDirectory,
  joinAndNormalizePath
}
