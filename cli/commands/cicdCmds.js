const { createPipeline, listPipelines, getPipeline, updatePipeline, deletePipeline } = require('../../lib/cicdManager')

function registerCICDCommands(program) {
  // Create a new CI/CD pipeline
  program
    .command('create-pipeline')
    .description('Create a new CI/CD pipeline')
    .option('-n, --name <name>', 'Name of the pipeline')
    .option('-s, --source <source>', 'Source repository for the pipeline')
    .action((options) => {
      createPipeline(options)
        .then(() => console.log('Pipeline created successfully'))
        .catch(err => console.error('Error creating pipeline:', err.message))
    })

  // List all CI/CD pipelines
  program
    .command('list-pipelines')
    .description('List all CI/CD pipelines')
    .action(() => {
      listPipelines()
        .then(pipelines => console.log('Pipelines:', pipelines))
        .catch(err => console.error('Error listing pipelines:', err.message))
    })

  // Get details of a specific CI/CD pipeline
  program
    .command('get-pipeline <id>')
    .description('Get details of a specific CI/CD pipeline')
    .action((id) => {
      getPipeline(id)
        .then(pipeline => console.log('Pipeline details:', pipeline))
        .catch(err => console.error('Error fetching pipeline details:', err.message))
    })

  // Update a specific CI/CD pipeline
  program
    .command('update-pipeline <id>')
    .description('Update a specific CI/CD pipeline')
    .option('-n, --name <name>', 'New name of the pipeline')
    .option('-s, --source <source>', 'New source repository for the pipeline')
    .action((id, options) => {
      updatePipeline(id, options)
        .then(() => console.log('Pipeline updated successfully'))
        .catch(err => console.error('Error updating pipeline:', err.message))
    })

  // Delete a specific CI/CD pipeline
  program
    .command('delete-pipeline <id>')
    .description('Delete a specific CI/CD pipeline')
    .action((id) => {
      deletePipeline(id)
        .then(() => console.log('Pipeline deleted successfully'))
        .catch(err => console.error('Error deleting pipeline:', err.message))
    })

  // Additional CI/CD commands can be added here...
}

module.exports = registerCICDCommands
