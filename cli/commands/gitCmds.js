const { 
  listRepositories, 
  createRepository, 
  deleteRepository, 
  updateRepository, 
  cloneRepository, 
  pullRepository, 
  pushRepository 
} = require('../../lib/gitManager')

function registerGitCommands(program) {
  program
    .command('list-repos')
    .description('List all Git repositories')
    .action(() => {
      listRepositories()
        .then(repos => {
          console.log('Repositories:', repos)
        })
        .catch(error => {
          console.error('Error listing repositories:', error)
        })
    })

  program
    .command('create-repo <name>')
    .description('Create a new Git repository')
    .option('-p, --private', 'Make the repository private')
    .action((name, options) => {
      createRepository(name, options.private)
        .then(response => {
          console.log('Repository created:', response)
        })
        .catch(error => {
          console.error('Error creating repository:', error)
        })
    })

  program
    .command('delete-repo <name>')
    .description('Delete a Git repository')
    .action(name => {
      deleteRepository(name)
        .then(() => {
          console.log('Repository deleted:', name)
        })
        .catch(error => {
          console.error('Error deleting repository:', error)
        })
    })

  program
    .command('update-repo <name>')
    .description('Update a Git repository settings')
    .option('-p, --private', 'Set the repository to private')
    .option('-d, --description <description>', 'Set the repository description')
    .action((name, options) => {
      updateRepository(name, options)
        .then(() => {
          console.log('Repository updated:', name)
        })
        .catch(error => {
          console.error('Error updating repository:', error)
        })
    })

  program
    .command('clone-repo <repository>')
    .description('Clone a Git repository')
    .option('-d, --destination <path>', 'Destination path')
    .action((repository, options) => {
      cloneRepository(repository, options.destination)
        .then(() => {
          console.log('Repository cloned:', repository)
        })
        .catch(error => {
          console.error('Error cloning repository:', error)
        })
    })

  program
    .command('pull-repo <repository>')
    .description('Pull updates from a Git repository')
    .action(repository => {
      pullRepository(repository)
        .then(() => {
          console.log('Repository updated:', repository)
        })
        .catch(error => {
          console.error('Error pulling repository:', error)
        })
    })

  program
    .command('push-repo <repository>')
    .description('Push changes to a Git repository')
    .action(repository => {
      pushRepository(repository)
        .then(() => {
          console.log('Changes pushed to repository:', repository)
        })
        .catch(error => {
          console.error('Error pushing to repository:', error)
        })
    })

  // More Git-related commands...
}

module.exports = registerGitCommands
