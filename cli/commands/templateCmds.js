const { listTemplates, createTemplate, getTemplate, updateTemplate, deleteTemplate } = require('../../lib/templateManager')

function registerTemplateCommands(program) {
  program
    .command('list-templates')
    .description('List all templates')
    .action(() => {
      listTemplates().then(templates => {
        console.log('Available Templates:')
        templates.forEach(template => console.log(template.name))
      })
    })

  program
    .command('create-template')
    .description('Create a new template')
    .option('-n, --name <name>', 'Name of the template')
    .option('-d, --description <description>', 'Description of the template')
    .action((cmd) => {
      const { name, description } = cmd
      createTemplate({ name, description }).then(() => {
        console.log(`Template '${name}' created successfully`)
      })
    })

  program
    .command('get-template <templateId>')
    .description('Get details of a specific template')
    .action((templateId) => {
      getTemplate(templateId).then(template => {
        console.log(`Details of Template: ${templateId}`)
        console.log(template)
      })
    })

  program
    .command('update-template <templateId>')
    .description('Update a specific template')
    .option('-n, --name <name>', 'New name of the template')
    .option('-d, --description <description>', 'New description of the template')
    .action((templateId, cmd) => {
      const { name, description } = cmd
      updateTemplate(templateId, { name, description }).then(() => {
        console.log(`Template '${templateId}' updated successfully`)
      })
    })

  program
    .command('delete-template <templateId>')
    .description('Delete a specific template')
    .action((templateId) => {
      deleteTemplate(templateId).then(() => {
        console.log(`Template '${templateId}' deleted successfully`)
      })
    })

  // Additional template-related commands can be added here...
}

module.exports = registerTemplateCommands
