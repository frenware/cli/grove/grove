const { createProject, listProjects, getProject, updateProject, deleteProject } = require('../../lib/projectManager')



// In projectCmds.js
const { createProject, listProjects } = require('../../lib/projectManager');

function registerProjectCommandsV1(program) {
  program
    .command('create-project')
    .description('Create a new project')
    .action(() => {
      // Call prompts here if needed, then call createProject()
    });

  program
    .command('list-projects')
    .description('List all projects')
    .action(() => {
      listProjects().then(projects => {
        console.log(projects);
      });
    });

  // More project-related commands...
}

module.exports = registerProjectCommands;






function registerProjectCommands(program) {
  program
    .command('create-project')
    .description('Create a new project')
    .option('-n, --name <name>', 'Name of the project')
    .option('-d, --description <description>', 'Description of the project')
    .action((options) => {
      const { name, description } = options
      if (!name) {
        console.error('Error: Project name is required')
        return
      }
      createProject({ name, description })
        .then(() => console.log(`Project ${name} created successfully`))
        .catch(err => console.error('Error creating project:', err))
    })

  program
    .command('list-projects')
    .description('List all projects')
    .action(() => {
      listProjects()
        .then(projects => {
          console.log('Projects:', projects)
        })
        .catch(err => console.error('Error listing projects:', err))
    })

  program
    .command('get-project <id>')
    .description('Get details of a specific project')
    .action((id) => {
      getProject(id)
        .then(project => {
          console.log('Project Details:', project)
        })
        .catch(err => console.error('Error getting project details:', err))
    })

  program
    .command('update-project <id>')
    .description('Update a specific project')
    .option('-n, --name <name>', 'New name of the project')
    .option('-d, --description <description>', 'New description of the project')
    .action((id, options) => {
      const { name, description } = options
      updateProject(id, { name, description })
        .then(() => console.log(`Project ${id} updated successfully`))
        .catch(err => console.error('Error updating project:', err))
    })

  program
    .command('delete-project <id>')
    .description('Delete a specific project')
    .action((id) => {
      deleteProject(id)
        .then(() => console.log(`Project ${id} deleted successfully`))
        .catch(err => console.error('Error deleting project:', err))
    })
}

module.exports = registerProjectCommands
