const settingsManager = require('../../lib/config/configManager')

function registerSettingsCommands(program) {
  program
    .command('view-settings')
    .description('View current Grove settings')
    .action(() => {
      settingsManager.viewSettings()
        .then(settings => {
          console.log('Current Settings:', settings)
        })
        .catch(err => {
          console.error('Error fetching settings:', err.message)
        })
    })

  program
    .command('set-setting <key> <value>')
    .description('Set a specific setting for Grove')
    .action((key, value) => {
      settingsManager.setSetting(key, value)
        .then(() => {
          console.log(`Setting updated: ${key} = ${value}`)
        })
        .catch(err => {
          console.error('Error updating setting:', err.message)
        })
    })

  program
    .command('reset-settings')
    .description('Reset Grove settings to their default values')
    .action(() => {
      settingsManager.resetSettings()
        .then(() => {
          console.log('Settings have been reset to default values')
        })
        .catch(err => {
          console.error('Error resetting settings:', err.message)
        })
    })

  // Add more settings-related commands here as needed
}

module.exports = registerSettingsCommands
