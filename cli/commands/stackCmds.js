const { listStacks, createStack, getStack, updateStack, deleteStack } = require('../../lib/stackManager')

function registerStackCommands(program) {
  program
    .command('list-stacks')
    .description('List all stack templates')
    .action(() => {
      listStacks().then(stacks => {
        console.log('Available Stack Templates:')
        stacks.forEach(stack => console.log(stack.name))
      })
    })

  program
    .command('create-stack')
    .description('Create a new stack template')
    .option('-n, --name <name>', 'Name of the stack')
    .option('-d, --description <description>', 'Description of the stack')
    .action((cmd) => {
      const { name, description } = cmd
      createStack({ name, description }).then(() => {
        console.log(`Stack template '${name}' created successfully`)
      })
    })

  program
    .command('get-stack <stackId>')
    .description('Get details of a specific stack template')
    .action((stackId) => {
      getStack(stackId).then(stack => {
        console.log(`Details of Stack Template: ${stackId}`)
        console.log(stack)
      })
    })

  program
    .command('update-stack <stackId>')
    .description('Update a specific stack template')
    .option('-n, --name <name>', 'New name of the stack')
    .option('-d, --description <description>', 'New description of the stack')
    .action((stackId, cmd) => {
      const { name, description } = cmd
      updateStack(stackId, { name, description }).then(() => {
        console.log(`Stack template '${stackId}' updated successfully`)
      })
    })

  program
    .command('delete-stack <stackId>')
    .description('Delete a specific stack template')
    .action((stackId) => {
      deleteStack(stackId).then(() => {
        console.log(`Stack template '${stackId}' deleted successfully`)
      })
    })

  // Additional stack-related commands can be added here...
}

module.exports = registerStackCommands
