const { listContainers, startContainer, stopContainer, removeContainer, listImages, removeImage } = require('../../lib/dockerManager')

function registerDockerCommands(program) {
	program
		.command('docker-containers')
		.description('List all Docker containers')
		.option('-a, --all', 'List all containers, including stopped ones')
		.action((cmdObj) => {
			const showAll = cmdObj.all || false
			listContainers(showAll).then(containers => {
				console.log('Docker Containers:', containers)
			})
		})

	program
		.command('docker-start <containerId>')
		.description('Start a Docker container')
		.action((containerId) => {
			startContainer(containerId).then(() => {
				console.log(`Container ${containerId} started successfully`)
			})
		})

	program
		.command('docker-stop <containerId>')
		.description('Stop a Docker container')
		.action((containerId) => {
			stopContainer(containerId).then(() => {
				console.log(`Container ${containerId} stopped successfully`)
			})
		})

	program
		.command('docker-remove <containerId>')
		.description('Remove a Docker container')
		.action((containerId) => {
			removeContainer(containerId).then(() => {
				console.log(`Container ${containerId} removed successfully`)
			})
		})

	program
		.command('docker-images')
		.description('List all Docker images')
		.action(() => {
			listImages().then(images => {
				console.log('Docker Images:', images)
			})
		})

	program
		.command('docker-rmi <imageId>')
		.description('Remove a Docker image')
		.action((imageId) => {
			removeImage(imageId).then(() => {
				console.log(`Image ${imageId} removed successfully`)
			})
		})

	// More Docker-related commands can be added here...
}

module.exports = registerDockerCommands
