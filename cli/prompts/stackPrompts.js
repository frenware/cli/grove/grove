const prompts = require('prompts')

function createProjectPrompt() {
  return prompts([
    {
      type: 'text',
      name: 'name',
      message: 'Project name:',
      validate: name => name ? true : 'Project name is required'
    },
    {
      type: 'text',
      name: 'description',
      message: 'Project description:',
      initial: 'A new Grove project'
    },
    {
      type: 'select',
      name: 'template',
      message: 'Select a project template:',
      choices: [
        { title: 'Basic', value: 'basic' },
        { title: 'LAMP Stack', value: 'lamp' },
        { title: 'MERN Stack', value: 'mern' },
        { title: 'JAMstack', value: 'jamstack' }
        // Add more stack templates as needed
      ],
      initial: 0
    },
    {
      type: 'toggle',
      name: 'private',
      message: 'Make this project private?',
      initial: false,
      active: 'Yes',
      inactive: 'No'
    },
    {
      type: 'list',
      name: 'members',
      message: 'Add team members (comma separated emails):',
      separator: ','
    }
    // Additional prompts can be added as needed
  ])
}

function updateProjectPrompt() {
  return prompts([
    {
      type: 'text',
      name: 'description',
      message: 'Update project description:'
    },
    {
      type: 'toggle',
      name: 'private',
      message: 'Change project visibility to private?',
      initial: false,
      active: 'Yes',
      inactive: 'No'
    }
    // Additional update prompts can be added as needed
  ])
}

module.exports = {
  createProjectPrompt,
  updateProjectPrompt
}
