const simpleGit = require('simple-git')
const path = require('path')

class GitManager {
  constructor(repoPath) {
    this.git = simpleGit(repoPath)
    this.repoPath = repoPath
  }

  async cloneRepository(repoUrl, branch = 'master') {
    try {
      await this.git.clone(repoUrl, this.repoPath, ['-b', branch])
      return { success: true, message: `Repository cloned from ${repoUrl}` }
    } catch (error) {
      return { success: false, message: error.message }
    }
  }

  async checkoutBranch(branchName) {
    try {
      await this.git.checkout(branchName)
      return { success: true, message: `Switched to branch ${branchName}` }
    } catch (error) {
      return { success: false, message: error.message }
    }
  }

  async createBranch(branchName) {
    try {
      await this.git.checkoutLocalBranch(branchName)
      return { success: true, message: `Branch ${branchName} created` }
    } catch (error) {
      return { success: false, message: error.message }
    }
  }

  async commitChanges(commitMessage) {
    try {
      await this.git.add('./*')
      await this.git.commit(commitMessage)
      return { success: true, message: `Changes committed: ${commitMessage}` }
    } catch (error) {
      return { success: false, message: error.message }
    }
  }

  async pushChanges(branchName) {
    try {
      await this.git.push('origin', branchName)
      return { success: true, message: `Changes pushed to branch ${branchName}` }
    } catch (error) {
      return { success: false, message: error.message }
    }
  }

  // Additional Git operations like merge, pull, fetch, etc., can be added here.
}

module.exports = GitManager
