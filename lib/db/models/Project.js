class Project {
  constructor({ id, name, description, stacks }) {
    this.id = id
    this.name = name
    this.description = description
    this.stacks = stacks || []
  }
}

module.exports = Project
