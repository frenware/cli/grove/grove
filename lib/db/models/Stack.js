class Stack {
  constructor({ id, name, type, configuration }) {
    this.id = id
    this.name = name
    this.type = type
    this.configuration = configuration
  }
}

module.exports = Stack
