
const dbManager1 = {
  async createProject(data) {
    const newProject = new Project(Date.now().toString(), data.name, data.description)
    await dbClient.put(`projects:${newProject.id}`, newProject)
    return newProject
  },

  async getProject(id) {
    return await dbClient.get(`projects:${id}`)
  },

  async updateProject(id, data) {
    const project = await this.getProject(id)
    Object.assign(project, data)
    await dbClient.put(`projects:${id}`, project)
    return project
  },

  async deleteProject(id) {
    await dbClient.delete(`projects:${id}`)
  },

  async createStack(projectId, data) {
    const newStack = new Stack(Date.now().toString(), data.name, data.templateId)
    await dbClient.put(`projects:${projectId}:stacks:${newStack.id}`, newStack)
    return newStack
  },

  async getStack(projectId, stackId) {
    return await dbClient.get(`projects:${projectId}:stacks:${stackId}`)
  },

  // ... more CRUD operations for Stack and other entities
}




const LevelDB = require('@plaindb/leveldb')
const Project = require('./models/Project')
const Stack = require('./models/Stack')

class DBManager {
  constructor() {
    this.db = new LevelDB()
  }
  // Project Operations
  async createProject(projectData) {
    const project = new Project(projectData)
    await this.db.put(`project:${project.id}`, project)
    return project
  }
  async getProject(projectId) {
    return await this.db.get(`project:${projectId}`)
  }
  async updateProject(projectId, updates) {
    let project = await this.getProject(projectId)
    project = { ...project, ...updates }
    await this.db.put(`project:${projectId}`, project)
    return project
  }
  async deleteProject(projectId) {
    await this.db.del(`project:${projectId}`)
  }
  // Stack Operations
  async createStack(stackData) {
    const stack = new Stack(stackData)
    await this.db.put(`stack:${stack.id}`, stack)
    return stack
  }
  async getStack(stackId) {
    return await this.db.get(`stack:${stackId}`)
  }
  async updateStack(stackId, updates) {
    let stack = await this.getStack(stackId)
    stack = { ...stack, ...updates }
    await this.db.put(`stack:${stackId}`, stack)
    return stack
  }
  async deleteStack(stackId) {
    await this.db.del(`stack:${stackId}`)
  }
  // ... More operations can be added as required
}

module.exports = DBManager
