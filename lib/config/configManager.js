const fs = require('fs')
const path = require('path')
const dotenv = require('dotenv')

class ConfigManager {
  constructor() {
    this.configFilePath = path.join(__dirname, '..', '.env')
    this.config = {}
    this.loadConfig()
  }
  loadConfig() {
    if (fs.existsSync(this.configFilePath)) {
      const envConfig = dotenv.parse(fs.readFileSync(this.configFilePath))
      for (const k in envConfig) {
        this.config[k] = envConfig[k]
      }
    }
  }
  getConfig(key) {
    return this.config[key]
  }
  setConfig(key, value) {
    this.config[key] = value
    this.saveConfig()
  }
  saveConfig() {
    const configData = Object.keys(this.config).map(key => `${key}=${this.config[key]}`).join('\n')
    fs.writeFileSync(this.configFilePath, configData)
  }
  validateConfig() {
    // Add validation logic for configuration settings.
    // Throw errors if validation fails.
    // Example:
    if (!this.config['REQUIRED_CONFIG']) {
      throw new Error('Required configuration missing: REQUIRED_CONFIG')
    }
  }
}

module.exports = new ConfigManager()
