const fs = require('fs')
const path = require('path')

const commonUtils = {
  /**
   * Asynchronously reads a file and returns its content.
   * @param {string} filePath - The path to the file.
   * @returns {Promise<string>} - A promise that resolves to the file content.
   */
  readFileAsync: async (filePath) => {
    return new Promise((resolve, reject) => {
      fs.readFile(filePath, 'utf8', (err, data) => {
        if (err) reject(err)
        else resolve(data)
      })
    })
  },

  /**
   * Writes data to a file asynchronously.
   * @param {string} filePath - The path to the file.
   * @param {string} data - The data to write.
   * @returns {Promise<void>}
   */
  writeFileAsync: async (filePath, data) => {
    return new Promise((resolve, reject) => {
      fs.writeFile(filePath, data, 'utf8', (err) => {
        if (err) reject(err)
        else resolve()
      })
    })
  },

  /**
   * Checks if a given file exists.
   * @param {string} filePath - The path to the file.
   * @returns {Promise<boolean>}
   */
  fileExistsAsync: async (filePath) => {
    return new Promise((resolve) => {
      fs.access(filePath, fs.constants.F_OK, (err) => {
        resolve(!err)
      })
    })
  },

  /**
   * Creates a directory recursively.
   * @param {string} dirPath - The path to the directory.
   * @returns {Promise<void>}
   */
  mkdirRecursiveAsync: async (dirPath) => {
    return new Promise((resolve, reject) => {
      fs.mkdir(dirPath, { recursive: true }, (err) => {
        if (err) reject(err)
        else resolve()
      })
    })
  },

  /**
   * Formats a date object into a readable string.
   * @param {Date} date - The date to format.
   * @returns {string}
   */
  formatDate: (date) => {
    return date.toISOString().replace(/T/, ' ').replace(/\..+/, '')
  }
}

module.exports = commonUtils
