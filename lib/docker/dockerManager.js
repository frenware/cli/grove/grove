const Docker = require('dockerode')
const docker = new Docker({ socketPath: '/var/run/docker.sock' })

class DockerManager {
  // List all containers
  async listContainers(all = false) {
    try {
      return await docker.listContainers({ all })
    } catch (error) {
      throw new Error(`Error listing Docker containers: ${error.message}`)
    }
  }

  // Start a container
  async startContainer(containerId) {
    try {
      const container = docker.getContainer(containerId)
      await container.start()
      return { message: `Container ${containerId} started successfully.` }
    } catch (error) {
      throw new Error(`Error starting container ${containerId}: ${error.message}`)
    }
  }

  // Stop a container
  async stopContainer(containerId) {
    try {
      const container = docker.getContainer(containerId)
      await container.stop()
      return { message: `Container ${containerId} stopped successfully.` }
    } catch (error) {
      throw new Error(`Error stopping container ${containerId}: ${error.message}`)
    }
  }

  // Remove a container
  async removeContainer(containerId) {
    try {
      const container = docker.getContainer(containerId)
      await container.remove()
      return { message: `Container ${containerId} removed successfully.` }
    } catch (error) {
      throw new Error(`Error removing container ${containerId}: ${error.message}`)
    }
  }

  // List all images
  async listImages() {
    try {
      return await docker.listImages()
    } catch (error) {
      throw new Error(`Error listing Docker images: ${error.message}`)
    }
  }

  // Pull an image
  async pullImage(imageName) {
    try {
      return new Promise((resolve, reject) => {
        docker.pull(imageName, (err, stream) => {
          if (err) {
            reject(`Error pulling image ${imageName}: ${err.message}`)
          }
          docker.modem.followProgress(stream, onFinished, onProgress)

          function onFinished(err, output) {
            if (err) {
              reject(`Error pulling image ${imageName}: ${err.message}`)
            }
            resolve({ message: `Image ${imageName} pulled successfully.`, output })
          }

          function onProgress(event) {
            console.log(event)
          }
        })
      })
    } catch (error) {
      throw new Error(`Error pulling image ${imageName}: ${error.message}`)
    }
  }

  // More functions for managing networks, volumes, Docker Compose stacks, etc. can be added here
}

module.exports = DockerManager
