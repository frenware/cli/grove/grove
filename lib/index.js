// lib/index.js

// Import individual modules
const DockerManager = require('./docker/dockerManager')
const GitManager = require('./git/gitManager')
const CiCdManager = require('./cicd/cicdManager')
const DbManager = require('./db/dbManager')
const ConfigManager = require('./config/configManager')
const CommonUtils = require('./utils/commonUtils')

// Initialize any required services or configurations
const dbClient = DbManager.initialize()
const config = ConfigManager.loadConfig()

// Export the modules for use in other parts of the application
module.exports = {
  Docker: new DockerManager(),
  Git: new GitManager(),
  CiCd: new CiCdManager(),
  Database: dbClient,
  Config: config,
  Utils: CommonUtils
}











// const Commander = require('./commander')

// class Executor {
//   constructor() {
//     this.commander = new Commander
//   }
//   async test(...args) {
//     return this.execute(['foo', 'sevn', ...args])
//   }
//   async execute(args) {
//     return this.commander.program.parseAsync(args)
//   }
//   static execute(args) {
//     const cli = new this
//     return cli.execute(args)
//   }
// }

// module.exports = Executor
