// Project Routes
app.get('/api/projects', projectController.getAllProjects);          // Get all projects
app.post('/api/projects', projectController.createProject);          // Create a new project
app.get('/api/projects/:projectId', projectController.getProject);    // Get a specific project
app.put('/api/projects/:projectId', projectController.updateProject); // Update a specific project
app.delete('/api/projects/:projectId', projectController.deleteProject); // Delete a specific project

// Stack Template Routes
app.get('/api/stacks', stackController.getAllStacks);                // Get all stack templates
app.post('/api/stacks', stackController.createStack);                // Create a new stack template
app.get('/api/stacks/:stackId', stackController.getStack);           // Get a specific stack template
app.put('/api/stacks/:stackId', stackController.updateStack);        // Update a specific stack template
app.delete('/api/stacks/:stackId', stackController.deleteStack);     // Delete a specific stack template

// Docker Service Routes
app.get('/api/docker/services', dockerController.getAllServices);    // Get all Docker services
app.post('/api/docker/services', dockerController.createService);    // Create a new Docker service
app.get('/api/docker/services/:serviceId', dockerController.getService); // Get a specific Docker service
app.put('/api/docker/services/:serviceId', dockerController.updateService); // Update a specific Docker service
app.delete('/api/docker/services/:serviceId', dockerController.deleteService); // Delete a specific Docker service

// Git Repository Routes
app.get('/api/git/repositories', gitController.getAllRepositories);  // Get all repositories
app.post('/api/git/repositories', gitController.createRepository);    // Create a new repository
app.get('/api/git/repositories/:repoId', gitController.getRepository); // Get a specific repository
app.put('/api/git/repositories/:repoId', gitController.updateRepository); // Update a specific repository
app.delete('/api/git/repositories/:repoId', gitController.deleteRepository); // Delete a specific repository

// CI/CD Pipeline Routes
app.get('/api/cicd/pipelines', cicdController.getAllPipelines);      // Get all CI/CD pipelines
app.post('/api/cicd/pipelines', cicdController.createPipeline);      // Create a new CI/CD pipeline
app.get('/api/cicd/pipelines/:pipelineId', cicdController.getPipeline); // Get a specific CI/CD pipeline
app.put('/api/cicd/pipelines/:pipelineId', cicdController.updatePipeline); // Update a specific CI/CD pipeline
app.delete('/api/cicd/pipelines/:pipelineId', cicdController.deletePipeline); // Delete a specific CI/CD pipeline

// Settings Routes
app.get('/api/settings', settingsController.getSettings);            // Get global settings
app.put('/api/settings', settingsController.updateSettings);         // Update global settings
